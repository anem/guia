# Listado de grados

¿Qué opciones tienes a la hora de estudiar matemáticas? En esta página te contamos qué estudios hay disponibles en universidades españolas, qué los diferencia y cuáles son sus salidas.

## Grado en matemáticas

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las siguientes universidades:
[Alicante](https://cvnet.cpd.ua.es/webcvnet/planestudio/planestudiond.aspx?plan=C052&lengua=C),
[Almería](http://cms.ual.es/UAL/estudios/grados/GRADO0410),
[Autònoma de Barcelona](http://www.uab.cat/web/estudiar/listado-de-grados/informacion-general/matematicas-1216708258897.html?param1=1216102918128),
[Autónoma de Madrid](http://www.uam.es/Ciencias/Matematicas/1242655568413.htm),
[Barcelona](http://www.ub.edu/web/ub/es/estudis/oferta_formativa/graus/fitxa/M/G1042/index.html),
[Cantabria](https://web.unican.es/centros/ciencias/grado/grado-en-matematicas),
[Cádiz](http://ciencias.uca.es/matematicas/),
[Complutense de Madrid](https://www.ucm.es/estudios/grado-matematicas-plan/),
[Extremadura](https://www.unex.es/conoce-la-uex/centros/ciencias/titulaciones/info/presentacion?id=0124),
[Granada](http://grados.ugr.es/matematicas/),
[Illes Balears](http://estudis.uib.es/es/grau/matematiques/GMAT-P/),
[La Laguna](https://www.ull.es/grados/matematicas/),
[La Rioja](https://www.unirioja.es/estudios/grados/matematicas/index.shtml),
[Málaga](https://www.uma.es/grado-en-matematicas),
[Murcia](http://www.um.es/web/matematicas/contenido/estudios/matematicas),
[Oviedo](http://www.uniovi.es/-/grado-en-matematicas-2013),
[País Vasco/EHU](https://www.ehu.eus/es/web/ztf-fct/grado-matematicas),
[Politècnica de Catalunya](https://fme.upc.edu/es/los-estudios/grados/grado-en-matematicas),
[Rey Juan Carlos](https://www.urjc.es/estudios/grado/1245-matematicas),
[Salamanca](http://www.usal.es/grado-en-matematicas),
[Sevilla](http://www.us.es/estudios/grados/plan_171),
[UNED](http://portal.uned.es/portal/page?_pageid=93,61703704&_dad=portal&_schema=PORTAL),
[València](https://www.uv.es/uvweb/universidad/es/estudios-grado/oferta-grados/oferta-grados/grado-matematicas-1285846094474/Titulacio.html?id=1285847387296),
[Valladolid](http://www.uva.es/export/sites/uva/2.docencia/2.01.grados/2.01.02.ofertaformativagrados/2.01.02.01.alfabetica/grado-en-matematicas/) y 
[Zaragoza](https://ciencias.unizar.es/grado-en-matematicas-0).

## Grado en estadística

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las siguientes universidades:
[Barcelona](http://www.ub.edu/web/ub/es/estudis/oferta_formativa/graus/fitxa/E/G1020/index.html),
[Extremadura](https://www.unex.es/conoce-la-uex/centros/ciencias/titulaciones/info/presentacion?id=0129),
[Granada](http://grados.ugr.es/estadistica/),
[Politècnica de Catalunya](https://www.upc.edu/es/grados/estadistica-interuniversitario-ub-upc-barcelona-fme),
[Salamanca](http://www.usal.es/grado-en-estadistica),
[Sevilla](http://www.us.es/estudios/grados/plan_196) y
[Valladolid](http://www.eio.uva.es/docencia/grado/).


## Grado en ciencia de datos

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las siguientes universidades:
Carlos III de Madrid,
Navarra,
Politècnica de Catalunya,
Politècnica de València y 
València.

## Doble grado en matemáticas y estadística

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las siguientes universidades:
[Complutense de Madrid](https://www.ucm.es/estudios/grado-matematicasyestadistica) y
[Sevilla](http://www.us.es/estudios/grados/plan_241).

## Doble grado en matemáticas e ingeniería informática

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las universidades de
[Murcia](http://www.universia.es/estudios/universidad-murcia/grado-matematicas-grado-ingenieria-informatica/st/252214),
[Politécnica de Madrid](http://www.universia.es/estudios/universidad-politecnica-madrid/grado-matematicas-informatica/st/181585), 
[Rey Juan Carlos](http://www.universia.es/estudios/universidad-rey-juan-carlos/doble-grado-ingenieria-informatica-matematicas/st/187183), 
[Complutense de Madrid](http://www.universia.es/estudios/universidad-complutense-madrid/doble-grado-ingenieria-informatica-matematicas/st/179925),

## Doble grado en matemáticas y física

**Qué se estudia:** en el doble grado estudiarás asignaturas de matemáticas y física, evitando duplicidades y, en algunos casos, profundizando en lo que tienen en común (por ejemplo, planteando las asignaturas de matemáticas desde el punto de vista de sus aplicaciones a la física, o viendo con especial rigor las matemáticas que hay detrás de muchos conceptos físicos). Es una buena opción si estás especialmente interesado en temas de física teórica y física matemática, o si te gustan las dos carreras y no tienes claro por qué decidirte.

**Qué salidas tiene:** más allá de las salidas de cada grado por separado, el doble grado es idóneo si quieres dedicarte a la investigación en física teórica. 

?> Se imparte en las siguientes universidades:
[Autònoma de Barcelona](http://www.uab.cat/web/estudiar/listado-de-grados/informacion-general/fisica-matematicas-1216708258897.html?param1=1216795185845), 
[Barcelona](https://mat.ub.edu/graumatesfisica/), 
[Cantabria](https://web.unican.es/centros/ciencias/grado/doble-grado-en-fisica-y-matematicas), 
[Complutense de Madrid](https://www.ucm.es/estudios/grado-matematicasyfisica), 
[Granada](http://grados.ugr.es/fisicamatematicas/pages/titulacion),
[Santiago de Compostela](http://www.usc.es/es/centros/matematicas/titulacions.html?plan=16616&estudio=16617&codEstudio=15968&valor=9), 
[Sevilla](http://www.us.es/estudios/grados/plan_240),
[Oviedo](http://www.uniovi.es/estudios/grados/-/asset_publisher/X5CYKURHdF1e/content/doble-grado-en-matematicas-y-fisica-2014),
[Valladolid](http://www.uva.es/export/sites/uva/2.docencia/2.01.grados/2.01.02.ofertaformativagrados/2.01.02.01.alfabetica/Programa-de-estudios-conjunto-de-Grado-en-Fisica-y-Grado-en-Matematicas/) y
[Zaragoza](https://ciencias.unizar.es/programa-conjunto-fisica-matematicas-fismat).

## Doble grado en estadística y economía

**Qué se estudia:**

**Qué salidas tiene:**

?> Se imparte en las universidades de 
[Barcelona](http://www.universia.es/estudios/universitat-barcelona/doble-grado-economia-estadistica/st/187390),
[Complutense de Madrid](http://www.universia.es/estudios/universidad-complutense-madrid/doble-grado-economia-matematicas-estadistica/st/258981),
[Carlos III de Madrid](http://www.universia.es/estudios/universidad-carlos-iii-madrid/grado-estadistica-empresa/st/115996).
