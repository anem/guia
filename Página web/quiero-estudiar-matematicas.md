# Quiero estudiar matemáticas

Has terminado la Selectividad o un grado medio, y quieres que tu futuro sea estudiar en la universidad. Pero, ¿cómo puedes asegurarte de que vas a tomar la decisión correcta a la hora de elegir la carrera? En esta página pretendemos no convencerte, sino ayudarte a que tomes la decisión con toda la información posible.

## La vocación

Hace unos años, si decías que querías estudiar matemáticas, probablemente la gente te miraba con mala cara.

> Pero eso para qué sirve, ¿para dar clase? ¿No prefieres estudiar ingeniería, que es más útil?

Afortunadamente, a día de hoy las matemáticas gozan de una alta estima, gracias en gran parte al surgimiento de palabras como *algoritmos*, *ciencia de datos* o *criptografía* en la prensa. Esto hace qe estudiar matemáticas se haya puesto de moda, y tenga tantas salidas que las matemáticas y la estadística están frecuentemente entre las profesiones con menor desempleo.

¿Significa esto que todo el mundo debería intentar estudiar matemáticas? Nada más lejos de la realidad. Las matemáticas son ante todo una carrera muy vocacional: si quieres sacarlas adelante no es necesario que se te den bien o que seas muy bueno, sino que disfrutes estudiándolas.

Por lo tanto, la primera pregunta que debes hacerte es: ¿voy a disfrutar pasando cuatro años estudiando matemáticas? (La segunda sería: ¿voy a ser feliz dedicándome a las matemáticas el resto de mi vida?).

Más allá de las matemáticas, nuestra recomendación es que estudies lo que quieras, sin importarte lo que digan de las salidas que tenga. Si disfrutas con ello, sabrás buscarte la vida.

## ¿Qué hace un matemático?

¡Muchas cosas! Por ejemplo:

- Empezando por lo fácil: ejercer de docente en institutos. Las matemáticas tienen un papel importantísimo en el desarrollo de la ciencia, y por lo tanto es importante que haya matemáticos bien formados impartiendo clases en secundaria y bachillerato.
- Investigar: las matemáticas crecen y avanzan cada día, gracias al trabajo de muchos hombres y mujeres que investigan en matemáticas, ya sea puras o aplicadas. En esta categoría suelen estar también la mayoría del profesorado universitario.
- En la empresa privada, crear modelos matemáticos para intentar hacer predicciones. Por ejemplo, es muy frecuente encontrar ofertas de trabajo en bancos o empresas de inversión.
- En ciencia de datos, buscar convertir la ingente cantidad de datos que se genera cada segundo en contenido con más sentido, y crear modelos para clasificar nuevas observaciones.
- Diseñar experimentos que sean coherentes y cuyos resultados sean significativos, es decir, que aporten valor a las investigaciones, utilizando para ello herramientas estadísticas.

En general, un matemático *resuelve problemas*. Por lo tanto, tu entrenamiento durante el grado no estará enfocado a qué hacer al salir, sino a ser capaz de enfrentarte a problemas o a situaciones desconocidas, y saber utilizar las herramientas a tu disposición para dar una solución óptima.

## Grado normal vs doble grado

Parece que lo tienes todo pensado, hasta que de repente lo ves: un doble grado con la carrera que quieres. ¿Qué deberías hacer?

Como siempre: lo que quieras. Pero te dejamos algunas cosas a tener en cuenta:

- Un doble grado son, en realidad, dos grados. Es decir, después de los cinco (o, en algunos casos, seis) años de estudio, recibirás dos títulos diferentes, por lo que esencialmente será como si hubieras estudiado los dos grados por separado, pero evitando todas las duplicidades.
- Los dobles grados tienen, inevitablemente, contenidos de dos áreas. Esto quiere decir que no sirven para estudiar una materia y complementarla con otra: en la práctica, aunque estudiarás todos los contenidos fundamentales de los dos grados, es posible que en los últimos años, cuando tienes que profundizar más y escoger optativas, el doble grado se quede corto frente al grado normal (por ejemplo, porque haya asignaturas con menos créditos, o porque no puedas estudiar todas las optativas que quieras).