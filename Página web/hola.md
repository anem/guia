# ¡Hola!

Te damos la bienvenida al mundo de las Matemáticas. Sí, tienes que tener presente que entras en un mundo nuevo, y lleno de posibilidades. En un grado al que has conseguido acceder después de tantos años de estudio de cosas que, tal vez, no eran tan de tu agrado. Así que, al fin, ahora empieza algo que has elegido.

![Mathematistan, por Martin Kuppe.](img/mathematistan.jpg?raw=true)

Durante los próximos años te verás envuelto de materias que (esperemos) te gustarán, y disfrutarás de esta etapa al máximo. Vívela todo lo que puedas, conoce gente nueva, gente afín; descubre nuevas cosas, deja atrás las que no te gusten… Todas las puertas se abren ante ti, para que cierres las que no quieras.

Y, de entre todas las puertas, la primera que se te abre es la más importante: las matemáticas. Dentro de esta etapa, que durará varios años, conocerás la enorme belleza que esconden y, puede más de una vez te sorprendan. Descubrirás de dónde vienen las cosas. Para que puedas contestar cuando te pregunten: ¿y eso para qué sirve?

Entrarás a este primer curso tal vez con unas expectativas algo diferentes, pero tendrás que tenerles paciencia a estas nuestras matemáticas, que a veces no dan una primera imagen en condiciones. Seguramente verás muchos cambios, muchas cosas nuevas de golpe; te darán nueva información que tendrás que asimilar en poco tiempo.

> En general no profundizamos en las matemáticas por sus aplicaciones prácticas. Lo hacemos por la misma razón por la que mucha gente devora literatura o sale a correr todas las mañanas: porque disfrutamos una barbaridad con ellas, y lo seguiremos haciendo sin tener que justificarnos por ello. <br> — *Miguel Ángel Morales*

Te aconsejamos que no te desesperes, es un cambio en la forma que tenías de ver las Matemáticas y te decimos más: investiga. Investiga las cosas que te llamen la atención, descubre qué utilidades puede tener,  motívate preguntando y buscando. Hasta podrás debatir con ese grupito que te has hecho sobre si lo que ha dicho el profesor está bien o no (y sobre si el 0 es natural, o no).

Busca las cosas que más te motiven, seguro que puedes acercarte a las Matemáticas más de lo que crees, y tirar de ellas para que te den lo que necesitas en esas clases. Y si crees que no lo vas a conseguir a la primera, no te preocupes, sólo hazlo a tu ritmo y busca lo mejor.