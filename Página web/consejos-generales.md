# Consejos generales

Más allá de lo que acabes estudiando, queremos darte también unas cuántas recomendaciones que creemos que te serán útiles para el futuro.

## Idiomas

Cuando entras a la Universidad vienes, probablemente, de llevar varios años estudiando una lengua extranjera (inglés, francés, alemán, etc.), y puede parecer tentador olvidarse del tema ahora que no es obligatorio. Pero nada más lejos de la realidad: seguir formándose y aprendiendo idiomas es muy importante no solo para tus estudios, sino también para tu futuro laboral.

En primer lugar, porque en la mayoría de universidades es necesario tener un título de nivel al menos B1 en alguna lengua extranjera para poder obtener el título de graduado, que sube a un B2 en los estudios de máster, y si te descuidas el tiempo se te acaba echando encima, y te plantas en tu último año sin título, lo cual puede suponer que apruebes todas las asignaturas de la carrera y no puedas graduarte porque tengas que estar a la espera de algún examen de idiomas. Y, cuando tengas el título y busques trabajo, te encontrarás con que la mayoría de empresas buscan a gente que pueda hablar en varios idiomas, algo que además te abre las puertas para trabajar en proyectos de ámbito internacional.

Además, y entrando ya en el caso más concreto de las matemáticas, el inglés es fundamental a la hora de buscar recursos online para estudiar. Apuntes, vídeos de explicaciones, ejercicios y exámenes resueltos, y mucho más. Y si puedes, no te quedes en el inglés: el francés o el alemán también son de mucha utilidad.

## Enfrentarse al fracaso

Mucha gente sale de Bachillerato y de Selectividad con unas notas muy buenas, y al llegar a los primeros meses del grado y enfrentarse a sus primeros exámenes, se desploman cuando no pueden mantener el nivel. Durante mucho tiempo te acostumbras a obtener muy buenos resultados, y ahora, pese a que te esfuerzas mucho más y te pasas el día en la biblioteca, suspendes o apruebas por los pelos.

Si es tu caso, queremos decirte alto y claro: ¡no pasa nada! Es normal que el salto a la universidad resulte difícil, y que te cueste más acostumbrarte al funcionamiento y a las asignaturas. En cualquier caso, es algo normal, y que seguramente le pasará al resto de estudiantes. Saques la nota que saques, tardes lo que tardes en aprobar, mantén la vista firme en tu objetivo y no te desanimes. Créenos: **valdrá la pena**.

Y, por el otro lado, si ves que te va bien, confía en es por tu capacidad, y no por suerte: algo llamado [síndrome del impostor](https://es.wikipedia.org/wiki/S%C3%ADndrome_del_impostor).