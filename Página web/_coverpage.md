<img src="img/logo/logo.svg" alt="Guía para estudiantes de matemáticas" height="480px" width="480px" data-no-zoom>

# **Guía** para **estudiantes** de **matemáticas**

> Recursos para quienes quieren empezar sus estudios universitarios en matemáticas o estadística

[Ver la guía](#guía-para-estudiantes-de-matemáticas)
[Contacto](contacto.md)
[Grupo de Telegram](http://bit.ly/quieroestudiarmates)