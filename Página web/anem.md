# La Asociación Nacional de Estudiantes de Matemáticas

Pues sí, ha llegado el momento del spam, pero es por una buena causa. Somos la ANEM, la Asociación Nacional de Estudiantes de Matemáticas, y nos encargamos de defender vuestros derechos y velar por vuestros intereses como estudiantes de matemáticas y/o estadística. Y somos estudiantes, como tú. 

La ANEM lleva más de 15 años luchando por los estudiantes. Actualmente colaboramos con muchos organismos como la Conferencia de Decanos de Matemáticas y la Real Sociedad Matemática Española, entre otros. Básicamente, somos representantes estudiantiles a escala nacional. Para que esta representación sea lo más efectiva posible y recoja las opiniones de todos los estudiantes posibles, dos veces al año nos reunimos en la Asamblea General. A ella acuden representantes de todas partes de España para marcar las pautas de trabajo de la Asociación y debatir sobre temas de relevancia en el panorama de las matemáticas, la
estadística y la educación. Si no sabes si tu universidad tiene representantes en la ANEM, infórmate y participa. 

?> Toda la información la tienes en [nuestra página web](https://www.anemat.com).

Además, también hacemos muchas otras más cosas:
- Tenemos un [Boletín cuatrimestral](https://www.anemat.com/boletin-anem-rsme/) conjunto con la Real Sociedad Matemática Española, que incluye información sobre actividades, becas y ofertas de trabajo, entrevistas y pasatiempos matemáticos.
- Hemos creado una revista de divulgación de trabajos de estudiantes de matemáticas, [TEMat](https://temat.es), iniciativa pionera a escala mundial.
- Organizamos un concurso de matemáticas a nivel nacional para el día de π.
- Mantenemos una [lista de másteres](https://www.anemat.com/masteres/) relacionados con las matemáticas a nivel nacional.

Por supuesto, también puedes seguirnos en [Facebook](https://www.facebook.com/ANEM.mat) y en [Twitter](https://twitter.com/ANEM_mat).

## El Encuentro Nacional de Estudiantes de Matemáticas

La última semana de cada julio tiene lugar el gran evento de la Asociación: el ENEM. En él asistentes de diferentes universidades pueden conocer gente con sus mismas inquietudes y aficiones, todos reunidos por un denominador común: las matemáticas.

?> Desde la primera edición en el año 2000, el ENEM ha recorrido todo el mapa de España: Granada (2000), Sevilla (2001), Oviedo (2002), Santiago de Compostela (2003), Alicante (2004), Cádiz (2005), Salamanca (2006), Granada (2007), Valencia (2008), Madrid (2009), Extremadura (2010), La Laguna (2011), Murcia (2012), Mallorca (2013), Málaga (2014), Salamanca (2015), Barcelona (2016), Sevilla (2017), Valencia (2018), Granada (2019).

Normalmente el ENEM gira en torno a sesiones de charlas, impartidas por investigadores y divulgadores de diferentes universidades. Podrías pensar que eso no tiene nada que ver con un estudiante de primero, ¿no? Pues te equivocas. La esencia del Encuentro reside en la gente y en las actividades que se organizan durante el mismo. Además de las charlas hay concursos con temática matemática (fotografía, relatos, etc.), visitas a centros de investigación, yincanas, talleres, un foro de empresas y actividades culturales y lúdicas.

![Los asistentes al XVIII ENEM Sevilla 2017, en el Ayuntamiento de Sevilla.](img/enem_sevilla.jpeg?raw=true)

Por lo tanto, durante el ENEM no solo complementamos nuestra formación, sino que conocemos a estudiantes de todo el país (incluso del extranjero) y tenemos una oportunidad única de compartir experiencias e intereses con ellos. De este encuentro salen amistades que duran toda la vida.

El ENEM se ha celebrado anualmente e ininterrumpidamente durante 19 ediciones. Si ha durado tanto, será por algo. ¡Te esperamos!