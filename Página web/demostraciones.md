# Demostraciones

Al llegar a la carrera de matemáticas afrontarás muchos problemas, teoremas, corolarios... todos ellos con su correspondiente demostración. Y aunque cuesta acostumbrarse a tener que demostrar todos los resultados que se utilizan, este proceso es en el fondo algo muy importante para todo matemático: todo lo que digas debe estar construído de manera lógica.

> **Demostración** (del latín *demonstratio, ōnis*): Prueba de algo, partiendo de verdades universales y evidentes. <br> — *Diccionario de la Lengua Española*

Todo esto está muy bien, pero en la práctica, ¿cómo puedes demostrar un resultado? Hay muchos métodos (demostración directa, por contrarrecíproco, por exhaución, por conteo o incluso en algunos casos con ayuda de ordenadores). Nosotros vamos a fijarnos solamente en dos: las demostraciones por inducción y las demostraciones por reducción al absurdo.

## Demostración por inducción

Sea $\mathbb{N}$ el conjunto de los números números naturales (es decir, $\mathbb{N} = \lbrace 1, 2, 3, \dots\rbrace$). Vamos a demostrar que la suma de los primeros $n$ números naturales es exactamente
$$ 1 + 2 + \cdots + n = \frac{n(n+1)}{2}.$$

1. En primer lugar, es fácil ver que es cierto para $n=1$. En efecto, 
$$1 = \frac{1 (1+1)}{2} = \frac{2}{2} = 1.$$
2. Supongamos que se cumple para $n$, y veamos que entonces también se cumplirá para $n+1$. En efecto,
$$ 1 + 2 + \cdots + (n+1) = (1 + 2 + \cdots + n) + (n+1). $$
Por nuestra suposición, la primera suma es exactamente $\frac{1}{2}n(n+1)$. Entonces
$$ 1 + 2 + \cdots + (n+1) = \frac{n(n+1)}{2} + (n+1) = (n+1)\left( \frac{n}{2} + 1 \right). $$
Simplificando la ecuación anterior se obtiene
$$ 1 + 2 + \cdots + (n+1) = \frac{n(n+1)}{2} + (n+1) = (n+1) \frac{(n+1) + 1}{2}. $$
Es decir, también se cumple lo previsto para $n+1$. Esto concluye la demostración.

Un momento, ¿cómo es que hemos acabado? Lo que sucede para que la demostración sea *válida* es similar a lo que pasa cuando pones en fila muchas fichas de dominó y empujas la primera: todas las demás van cayendo. Por ejemplo, ¿por qué es válida la fórmula para $n = 7$? A la vista del segundo paso, bastaría en realidad comprobar que se cumple en el caso $n = 6$. Pero para $n = 6$ bastaría entonces comprobar el caso $n=5$. Y siguiendo este proceso, llegamos a que gracias al segundo paso solo hay que comprobar el caso $n=1$, que hemos visto que es cierto. Por lo tanto, la fórmula se cumple para $7$ (y, en general, para cualquier valor de $n$).

## Demostración por reducción al absurdo

¿Quieres demostrar que algo es cierto? Entonces una estrategia muy útil es suponer que es falso y llegar a una contradicción. Y como es absurdo llegar a una contradicción, el error tiene que ser haber supuesto que lo que querías demostrar era falso.

?> Si asumes que algo absurdo es cierto, puedes demostrar cualquier cosa. Por ejemplo, si 0 = 1, entonces 1 = 2, lo que significa que el conjunto formado por el matemático Bertrand Rusell y el Papa tiene un único elemento. Por lo tanto, la conclusión es que Bertrand Rusell es el Papa.

Vamos con un ejemplo: demostremos que $\sqrt{2}$ es irracional. Supongamos lo contrario, es decir, que $\sqrt{2}$ es un número racional y lo podemos escribir como $\sqrt{2} = a/b$, donde $a$ y $b$ no tienen factores comunes (es decir, lo escribimos como una fracción lo más reducida posible). Entonces podemos decir
$$ 2 = \frac{a^2}{b^2}, \text{ es decir, } 2b^2 = a^2. $$
A la izquierda de la última igualdad hay un número par, por lo tanto a la derecha debe haberlo, es decir, $a^2$ es par. Esto significa que $a$ también debe ser par, y por lo tanto podemos reescribir $a = 2c$, luego
$$ 2 b^2 = (2c)^2 = 4 c^2 \Rightarrow 2c^2 = b^2. $$
Repetimos entonces ahora el mismo argumento: $b^2$ es par, y por lo tanto $b$ también. Pero, un momento: esto significa que tanto $a$ como $b$ son números pares, por lo tanto tienen un divisor común (el $2$). Esto contradice lo que habíamos supuesto al principio (que $a/b$ era una fracción reducida), con lo que llegamos a una contradicción. Y la única posibilidad es que la contradicción esté en suponer que $\sqrt{2} = a/b$, por lo tanto deducimos que la raíz cuadrada de dos debe ser irracional.

## Problemas para practicar

Demostraciones por inducción:

- $1 + 2 + 2^2 + \cdots + 2^n = 2^{n-1} - 1$.
- $1 + 3 + 5 + \cdots + (2n-1) = n^2$.
- $(a-b)$ divide a $a^n - b^n$ para todas las parejas de $a$ y $b$ enteros.

?> Uno con trampa: vamos a demostrar que todos los caballos son del mismo color. Si hubiera un único caballo, es evidente que sería de su color, por lo tanto se cumple el caso $n = 1$. Y si se cumple para $k$ caballos y aparece uno nuevo, podemos tomar dos grupos de $k$ caballos que deberán ser del mismo color. Y como hay caballos que están en los dos grupos, al final resulta que los $k + 1$ caballos son del mismo color. Pero esto es (evidentemente) falso: ¿qué es lo que ha fallado?

Demostraciones por reducción al absurdo:

- Hay infinitos números primos.
- La suma de un número racional y otro irracional es siempre irracional.
- Si $a$ y $b$ son números enteros, entonces $a^2 - 4b \neq 2$.

Otros problemas interesantes:

- Es fácil cubrir un tablero de ajedrez con fichas de dominó, de manera que cada ficha ocupe exáctamente dos casillas. Pero, ¿sigue siendo posible si eliminamos dos esquinas opuestas del tablero?
- Si seleccionamos aleatoriamente un grupo de personas y contamos el número de amigos que cada uno tiene dentro del grupo, habrá dos personas con el mismo número de amigos.

<script> MathJax.Hub.Typeset() </script>