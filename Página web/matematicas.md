# ¿Qué son las matemáticas?

Ahora que has entrado en la titulación de matemáticas lo primero que necesitas saber es qué la ciencia y cuáles son sus ramas. Desde los tiempos en la antigua Grecia, el ser humano siempre ha buscado la verdad y el conocimiento del universo y en esta búsqueda se ha planteado cuál es la mejor forma de conocer. Los pensadores griegos encontraron en la razón una fuente de sabiduría y se dieron cuenta que necesitaban un conjunto de reglas que dirigiese esa búsqueda. A partir del siglo XVII, apareció el método que definía estas reglas y, con él, la ciencia actual.

El origen de la palabra ciencia viene del latín, y su significado es “Conocimiento o saber de varios campos”. La ciencia se puede dividir en dos apartados: ciencia empírica (la cual a su vez se divide en natural y social) y ciencia formal, que es en la que nos centraremos nosotros como futuros matemáticos que somos. Una ciencia formal es aquella que establece unos razonamientos lógicos y que trabaja con ideas creadas por la mente (estilo Charles Xavier). Se basa en el razonamiento, y no en la experimentación; por lo que primero pensamos, y ya si eso sirve para algo o no… eso es otra cosa que no nos suele interesar.

?> Hay muchas diferencias entre ciencias formales y empíricas: el objeto de estudio (ideas frente a hechos), el método de análisis (inducción, deducción y lógica frente al método científico) o las comprobaciones que se realizan (razonamiento frente a experimentación).

Las ciencias formales se construyen en base a una serie de axiomas y definiciones, de las cuales se deducen teoremas. De ahí la importancia de las demostraciones, son el medio para pasar de estos axiomas a los teoremas.

## Axiomas

### Los axiomas de Peano

### Los axiomas de un grupo


## ¿Dónde puedes ver matemáticas?

