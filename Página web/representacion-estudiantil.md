# Representación estudiantil

¿Crees que sólo hay una forma de participar en la vida universitaria? ¿Recuerdas las elecciones a delegados de clase en el instituto? Sí, esa a la que sacábais al más guay de vuestro curso; porque las risas estaban echadas.

Pues hay algo más que unas normas escritas en un papel que velan por tus derechos, y son tus compañeros, aquellos que te representan de alguna manera frente al todos los problemas que puedan surgir, aquellos que saben de cualquier cosa relacionada con la universidad y, si no saben, lo buscan. Tú puedes ser uno de esos compañeros. Además, como representante de estudiantes, tendrás la oportunidad de adquirir muchísimos conocimientos transversales que, de otro modo, no podrías conseguir. A parte, tendrás la oportunidad de mejorar la universidad y aportar tu granito de arena para mejorar la sociedad.

A lo mejor no te interesa formar parte de estos grupos, pero sí es importante que sepas que están y que no le quites la importancia que tienen, ya que siempre están ahí para ayudarte.