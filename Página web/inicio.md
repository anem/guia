# Guía para estudiantes de matemáticas

En esta página web hemos intentado recopilar información y recursos de utilidad para quienes quieran empezar sus estudios universitarios en matemáticas o en estadística.

A la izquierda puedes navegar entre todas las categorías. Te recomendamos que las leas todas (aunque sea por encima). Si te quedan dudas o crees que hay algo que no está claro, [no dudes en comentárnoslo](contacto.md).

Encima del menú hay también un panel de búsqueda, para que encuentres la información más fácilmente. Y, si haces clic en los títulos y los subtítulos de las páginas

##### (por ejemplo, este),

puedes obtener un enlace directo a la sección para compartirlo o guardarlo.

Si te apetece contribuir a la guía, consulta las pautas correspondientes.