# La vida universitaria

Estás metiéndote de lleno en una nueva etapa de tu vida, en la que no sólo te formarás en la materia; sino también como persona, y adquirirás muchas cualidades. Pues la vida en la universidad no se queda estancada en las clases, estudiar y hacer exámenes; tienes muchas oportunidades en este nuevo cambio. La universidad es aprendizaje, cultura, relaciones, ocio y diversión. A parte de muchas más cosas que alomejor se nos escapan a nosotros. 

Hay muchísimas actividades, relacionadas o no con el ámbito de las matemáticas, que te están esperando. Teatro, cine, juegos, música, festivales, deportes… Esta época de tu vida estará repleta de cosas para hacer y que te aconsejamos disfrutes al máximo. Porque, ¿alguna vez has escuchado eso de que la vida universitaria es una de las mejores? Lo será si así te lo propones, pues no te puedes quedar en las clases o exámenes, todo te está esperando para que lo visites. Como decíamos antes, muchas puertas abiertas ante ti, esta etapa está para vivirla y disfrutarla.

Pero un primer consejo que te damos antes que nada: ¡Duerme!, y no te saltes comidas. Lo agradecerás a lo largo del curso. Desde luego, podrás crear esos lazos de los que puedas estar hablando durante siglos. ¡Atrévete a conocer gente nueva! Dale una oportunidad a esas personas que no creerías posible acercarte, te puede llegar a sorprender.

## Recursos

La universidad es una fuente inagotable de recursos que están a tu entera disposición y que te pueden ayudar mucho en tus estudios y organización. Así que es importante tenerlo todo situado. 

- Biblioteca: horarios, localización, posibilidad de salas de estudio, ordenadores y acceso a internet. Todo ello has de averiguarlo, muy sencillo, desplázate hasta ella o pregunta. Si eres de estudiar en la biblio, es imprescindible. Además a la hora de hacer trabajos será muy útil que tengas nociones básicas sobre préstamo de libros, acceso a material de consulta online…

- Servicios informáticos: será la plataforma en la que subirán apuntes, fechas importantes, avisos… Cada universidad trabaja con una distinta y los nombres son muy variados. Familiarízate con ella y consúltala con frecuencia.

- Correo: tendrás un correo propio para asuntos de la universidad. No lo dejes de lado, muchos profesores prefieren este método que las plataformas, así que ojo. Puede que te llegue mucho spam de la universidad, pero es imprescindible que lo leas, al menos, una vez al día.

- Coordinador del grado: es una figura poco conocida pero a la que debes de intentar sacarle el mayor partido posible. Su papel es asesorarte en todos los temas relativos a la carrera: optativas, prácticas, modo de enfocar la asignatura… Recurre a ellos siempre que puedas y mantén el contacto, nunca se sabe cuándo vas a necesitar un consejillo.

?> Recuerda que la universidad está ahí para ayudarte en todos los problemas, incluyendo los académicos (con programas de mentoría o tutorías) pero también los personales, por ejemplo con servicios de ayuda psicológica. La salud mental suele ignorarse a pesar de ser tan importante como la física, y no debes dejarla de lado.

Intenta tener ubicados todos estos servicios, y todo te será mucho más fácil. Y por supuesto, no dudes en pedir ayuda siempre que la necesites.