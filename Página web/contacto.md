# Contacta con nosotros

Esta página está mantenida por la Asociación Nacional de Estudiantes de Matemáticas. Para contactar con nosotros, puedes hablaros por:
- Correo electrónico: [contacto@anemat.com](mailto:contacto@anemat.com).
- Twitter: [@ANEM_mat](https://twitter.com/anem_mat).
- Facebook: [Asociación Nacional de Estudiantes de Matemáticas](https://www.facebook.com/anem.mat).

## Telegram

Si lo que tienes es dudas sobre los estudios de matemáticas, existe un grupo de Telegram en el que puedes preguntar tus dudas. Este grupo se creó por iniciativa de [Pedro Daniel Pajares](https://medium.com/atodogauss/14a60ee4aaf9), y cuenta ya con más de 90 miembros. [Puedes acceder al grupo haciendo clic aquí](http://bit.ly/quieroestudiarmates) (necesitarás tener instalada la aplicación de [Telegram](https://telegram.org/)).

Y si quieres, [en este otro enlace](http://bit.ly/voluntariosmates) puedes ver una lista de estudiantes de matemáticas de diferentes universidades que pueden echarte un cable con tus dudas.