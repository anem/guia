# Guía para estudiantes de matemáticas

En este repositorio pueden encontrarse los archivos tanto para la versión web como para el folleto para impresión de la Guía para estudiantes de matemáticas, una iniciativa de la Asociación Nacional de Estudiantes de Matemáticas destinada a ayudar a quienes comienzan sus estudios universitarios en matemáticas o estadística.

## Estructura

El repositorio contiene dos carpetas: una para la versión web y otra para el folleto informativo, destinado a imprimirse y repartirse de manera física. La página web puede visitarse en [el siguiente enlace](https://www.anemat.com/guia). Con respecto al folleto, si estás interesado en imprimir tu propia edición, tienes más información en el repositorio. Si prefieres que la ANEM te los envíe directamente, [contacta con nosotros](mailto:contacto@anemat.com) e intentaremos ayudarte.

## Licencia

Todo el contenido de este repositorio está licenciado con una Licencia MIT, que puedes consultar aquí. Esto significa, esencialmente, que puedes utilizar todo lo que ponemos a tu disposición de manera libre, sin necesidad de atribución (aunque no nos quejaremos). A cambio, lo que utilizes viene sin ningún tipo de garantías.

## Colaborar

Si quieres echarnos una mano o crees que hay secciones que podrían mejorar, ponte en contacto con nosotros [por correo electrónico](mailto:contacto@anemat.com), o haz una pull request.